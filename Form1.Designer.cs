﻿namespace ShutDownNow
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnShutDown = new System.Windows.Forms.Button();
            this.btnSetTime = new System.Windows.Forms.Button();
            this.lblON = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtHourAT = new System.Windows.Forms.TextBox();
            this.txtMinAT = new System.Windows.Forms.TextBox();
            this.txtSecAT = new System.Windows.Forms.TextBox();
            this.txtSecIN = new System.Windows.Forms.TextBox();
            this.txtMinIN = new System.Windows.Forms.TextBox();
            this.txtHourIN = new System.Windows.Forms.TextBox();
            this.rbAT = new System.Windows.Forms.RadioButton();
            this.rbIN = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnShutDown
            // 
            this.btnShutDown.Location = new System.Drawing.Point(275, 9);
            this.btnShutDown.Name = "btnShutDown";
            this.btnShutDown.Size = new System.Drawing.Size(90, 45);
            this.btnShutDown.TabIndex = 0;
            this.btnShutDown.Text = "Shut down";
            this.btnShutDown.UseVisualStyleBackColor = true;
            this.btnShutDown.Click += new System.EventHandler(this.BtnShutDown_Click);
            // 
            // btnSetTime
            // 
            this.btnSetTime.Location = new System.Drawing.Point(275, 60);
            this.btnSetTime.Name = "btnSetTime";
            this.btnSetTime.Size = new System.Drawing.Size(90, 45);
            this.btnSetTime.TabIndex = 1;
            this.btnSetTime.Text = "Set time";
            this.btnSetTime.UseVisualStyleBackColor = true;
            this.btnSetTime.Click += new System.EventHandler(this.BtnSetTime_Click);
            // 
            // lblON
            // 
            this.lblON.AutoSize = true;
            this.lblON.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblON.Location = new System.Drawing.Point(12, 12);
            this.lblON.Name = "lblON";
            this.lblON.Size = new System.Drawing.Size(255, 29);
            this.lblON.TabIndex = 2;
            this.lblON.Text = "Shut down time is set";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(275, 111);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(90, 45);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // txtHourAT
            // 
            this.txtHourAT.Location = new System.Drawing.Point(138, 95);
            this.txtHourAT.Name = "txtHourAT";
            this.txtHourAT.Size = new System.Drawing.Size(30, 22);
            this.txtHourAT.TabIndex = 5;
            this.txtHourAT.TextChanged += new System.EventHandler(this.TxtHourAT_TextChanged);
            // 
            // txtMinAT
            // 
            this.txtMinAT.Location = new System.Drawing.Point(184, 95);
            this.txtMinAT.Name = "txtMinAT";
            this.txtMinAT.Size = new System.Drawing.Size(30, 22);
            this.txtMinAT.TabIndex = 6;
            this.txtMinAT.TextChanged += new System.EventHandler(this.TxtMinAT_TextChanged);
            // 
            // txtSecAT
            // 
            this.txtSecAT.Location = new System.Drawing.Point(230, 95);
            this.txtSecAT.Name = "txtSecAT";
            this.txtSecAT.Size = new System.Drawing.Size(30, 22);
            this.txtSecAT.TabIndex = 7;
            this.txtSecAT.TextChanged += new System.EventHandler(this.TxtSecAT_TextChanged);
            // 
            // txtSecIN
            // 
            this.txtSecIN.Location = new System.Drawing.Point(230, 134);
            this.txtSecIN.Name = "txtSecIN";
            this.txtSecIN.Size = new System.Drawing.Size(30, 22);
            this.txtSecIN.TabIndex = 10;
            this.txtSecIN.Text = "00";
            this.txtSecIN.TextChanged += new System.EventHandler(this.TxtSecIN_TextChanged);
            // 
            // txtMinIN
            // 
            this.txtMinIN.Location = new System.Drawing.Point(184, 134);
            this.txtMinIN.Name = "txtMinIN";
            this.txtMinIN.Size = new System.Drawing.Size(30, 22);
            this.txtMinIN.TabIndex = 9;
            this.txtMinIN.Text = "00";
            this.txtMinIN.TextChanged += new System.EventHandler(this.TxtMinIN_TextChanged);
            // 
            // txtHourIN
            // 
            this.txtHourIN.Location = new System.Drawing.Point(138, 134);
            this.txtHourIN.Name = "txtHourIN";
            this.txtHourIN.Size = new System.Drawing.Size(30, 22);
            this.txtHourIN.TabIndex = 8;
            this.txtHourIN.Text = "01";
            this.txtHourIN.TextChanged += new System.EventHandler(this.TxtHourIN_TextChanged);
            // 
            // rbAT
            // 
            this.rbAT.Checked = true;
            this.rbAT.Location = new System.Drawing.Point(19, 95);
            this.rbAT.Name = "rbAT";
            this.rbAT.Size = new System.Drawing.Size(115, 21);
            this.rbAT.TabIndex = 11;
            this.rbAT.TabStop = true;
            this.rbAT.Text = "Shut down at:";
            this.rbAT.UseVisualStyleBackColor = true;
            this.rbAT.CheckedChanged += new System.EventHandler(this.RbAT_CheckedChanged);
            // 
            // rbIN
            // 
            this.rbIN.AutoSize = true;
            this.rbIN.Location = new System.Drawing.Point(19, 134);
            this.rbIN.Name = "rbIN";
            this.rbIN.Size = new System.Drawing.Size(114, 21);
            this.rbIN.TabIndex = 12;
            this.rbIN.Text = "Shut down in:";
            this.rbIN.UseVisualStyleBackColor = true;
            this.rbIN.CheckedChanged += new System.EventHandler(this.RbIN_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(170, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(216, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(12, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = ":";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(170, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(12, 17);
            this.label3.TabIndex = 15;
            this.label3.Text = ":";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(216, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(12, 17);
            this.label4.TabIndex = 16;
            this.label4.Text = ":";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 169);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rbIN);
            this.Controls.Add(this.rbAT);
            this.Controls.Add(this.txtSecIN);
            this.Controls.Add(this.txtMinIN);
            this.Controls.Add(this.txtHourIN);
            this.Controls.Add(this.txtSecAT);
            this.Controls.Add(this.txtMinAT);
            this.Controls.Add(this.txtHourAT);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblON);
            this.Controls.Add(this.btnSetTime);
            this.Controls.Add(this.btnShutDown);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnShutDown;
        private System.Windows.Forms.Button btnSetTime;
        private System.Windows.Forms.Label lblON;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtHourAT;
        private System.Windows.Forms.TextBox txtMinAT;
        private System.Windows.Forms.TextBox txtSecAT;
        private System.Windows.Forms.TextBox txtSecIN;
        private System.Windows.Forms.TextBox txtMinIN;
        private System.Windows.Forms.TextBox txtHourIN;
        private System.Windows.Forms.RadioButton rbAT;
        private System.Windows.Forms.RadioButton rbIN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

