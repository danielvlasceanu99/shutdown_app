﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Timers;

namespace ShutDownNow
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            txtHourAT.Text = Convert.ToString(DateTime.Now.Hour + 1);
            txtMinAT.Text = Convert.ToString(DateTime.Now.Minute);
            txtSecAT.Text = Convert.ToString(DateTime.Now.Second);
        }

        //Check and uncheck the shuting down method;
        bool inCheck;
        bool atCheck;
        private void RbAT_CheckedChanged(object sender, EventArgs e)
        {
            atCheck = rbAT.Checked;
        }
        private void RbIN_CheckedChanged(object sender, EventArgs e)
        {
            inCheck = rbIN.Checked;
        }
        private void RbAT_Click(object sender, EventArgs e)
        {
            if (!rbAT.Checked && !atCheck)
            {
                rbAT.Checked = true;
            }
        }
        private void RbIN_Click(object sender, EventArgs e)
        {
            if (!rbIN.Checked && !inCheck)
            {
                rbIN.Checked = true;
            }
        }
       

        //Initialize the time boxes
        int hourIN = 1;
        int minIN = 0;
        int secIN = 0;
        int hourAT = Convert.ToInt32(DateTime.Now.Hour + 1);
        int minAT = Convert.ToInt32(DateTime.Now.Minute);
        int secAT = Convert.ToInt32(DateTime.Now.Second);

        //Time data validations
        private void TxtHourAT_TextChanged(object sender, EventArgs e)
        {
            try
            {
                hourAT = Convert.ToInt32(txtHourAT.Text);
            }
            catch
            {
                MessageBox.Show("Please enter a valid value.", "Mesaj", MessageBoxButtons.OK);
            }
            if (hourAT > 23)
            {
                MessageBox.Show("Please enter a valid value.", "Mesaj", MessageBoxButtons.OK);
            }
        }
        private void TxtMinAT_TextChanged(object sender, EventArgs e)
        {
            try
            {
                minAT = Convert.ToInt32(txtMinAT.Text);
            }
            catch
            {
                MessageBox.Show("Please enter a valid value.", "Mesaj", MessageBoxButtons.OK);
            }
            if (minAT > 59)
            {
                MessageBox.Show("Please enter a valid value.", "Mesaj", MessageBoxButtons.OK);
            }
        }
        private void TxtSecAT_TextChanged(object sender, EventArgs e)
        {
            try
            {
                secAT = Convert.ToInt32(txtSecAT.Text);
            }
            catch
            {
                MessageBox.Show("Please enter a valid value.", "Mesaj", MessageBoxButtons.OK);
            }
            if (secAT > 59)
            {
                MessageBox.Show("Please enter a valid value.", "Mesaj", MessageBoxButtons.OK);
            }
        }
        private void TxtHourIN_TextChanged(object sender, EventArgs e)
        {
            try
            {
                hourIN = Convert.ToInt32(txtHourIN.Text);
            }
            catch
            {
                MessageBox.Show("Please enter a valid value.", "Mesaj", MessageBoxButtons.OK);
            }
        }
        private void TxtMinIN_TextChanged(object sender, EventArgs e)
        {
            try
            {
                minIN = Convert.ToInt32(txtMinIN.Text);
            }
            catch
            {
                MessageBox.Show("Please enter a valid value.", "Mesaj", MessageBoxButtons.OK);
            }
            if (minIN > 59)
            {
                MessageBox.Show("Please enter a valid value.", "Mesaj", MessageBoxButtons.OK);
            }
        }
        private void TxtSecIN_TextChanged(object sender, EventArgs e)
        {
            try
            {
                secIN = Convert.ToInt32(txtSecIN.Text);
            }
            catch
            {
                MessageBox.Show("Please enter a valid value.", "Mesaj", MessageBoxButtons.OK);
            }
            if (secIN > 59)
            {
                MessageBox.Show("Please enter a valid value.", "Mesaj", MessageBoxButtons.OK);
            }
        }

        //setting the timer
        static System.Timers.Timer timer;
        void schedule_Timer()
        {
            DateTime nowTime = DateTime.Now;
            if (rbAT.Checked)
            {
                DateTime scheduledTime = new DateTime(nowTime.Year, nowTime.Month, nowTime.Day, hourAT, minAT, secAT, 0);
                if (nowTime > scheduledTime)
                {
                    scheduledTime = scheduledTime.AddDays(1);
                }
                double tickTime = (double)(scheduledTime - DateTime.Now).TotalMilliseconds;
                timer = new System.Timers.Timer(tickTime);
                timer.Elapsed += new ElapsedEventHandler(Shutdown);
                timer.Start();
            }
            if (rbIN.Checked)
            {
                DateTime scheduledTime = new DateTime(nowTime.Year, nowTime.Month, nowTime.Day, nowTime.Hour, nowTime.Minute, nowTime.Second, 0);
                scheduledTime = scheduledTime.AddHours(hourIN);
                scheduledTime = scheduledTime.AddMinutes(minIN);
                scheduledTime = scheduledTime.AddSeconds(secIN);
                double tickTime = (double)(scheduledTime - DateTime.Now).TotalMilliseconds;
                timer = new System.Timers.Timer(tickTime);
                timer.Elapsed += new ElapsedEventHandler(Shutdown);
                timer.Start();
            }
        }

       

        //Shutdown functions;
        static void ShutdownNOW()
        {
            Process.Start("shutdown.exe", "-s -t 00");
        }
        static void Shutdown(object sender, ElapsedEventArgs e)
        {
            Process.Start("shutdown.exe", "-s -t 00");
        }
        //Test function;

        void TiemSet_message()
        {
            lblON.Text = "Timer set";
        }
        //Buttons initialization;
        private void BtnShutDown_Click(object sender, EventArgs e)
        {
            ShutdownNOW();
        }
        private void BtnSetTime_Click(object sender, EventArgs e)
        {
            TiemSet_message();
            schedule_Timer();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            timer.Stop();
            System.Environment.Exit(0);
        }
    }
}
